package com.soumio.inceptiontutorial.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.soumio.inceptiontutorial.R;
import com.soumio.inceptiontutorial.data.DBHelper;
import com.soumio.inceptiontutorial.model.Food;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddFoodActivity
        extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener
{

    // for permission requests
    public static final int REQUEST_PERMISSION = 300;
    // request code for permission requests to the os for image
    public static final int REQUEST_IMAGE = 100;
    // request code for choosing name of food
    public static final int REQUEST_NAME = 600;


    // will hold uri of image obtained from camera
    private Uri imageUri;
    private Food food;

    private ImageView foodImage;
    private TextView nameTV;
    private TextView dateTV;
    private TextView timeTV;
    private Button dateTimeBtn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);
        food = new Food(0L, getString(R.string.name_of_food), Calendar.getInstance());
        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_food, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_btn_save){
            Intent intent = new Intent();
            intent.putExtra("id", food.getId());
            intent.putExtra("name", food.getName());
            intent.putExtra("date", food.getDate().getTimeInMillis());
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        dateTimeBtn = findViewById(R.id.btn_add_date_time);
        foodImage = findViewById(R.id.iv_add_food_image);
        nameTV = findViewById(R.id.tv_add_food_name);
        dateTV = findViewById(R.id.tv_add_food_date);
        timeTV = findViewById(R.id.tv_add_food_time);
        foodImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions();
                showCameraDialog();
            }
        });
        dateTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDataTime();
            }
        });

        updateUI();
    }

    private void updateUI() {
        if (imageUri != null){
            foodImage.setImageURI(imageUri);
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        nameTV.setText(food.getName());
        dateTV.setText(dateFormatter.format(food.getDate().getTime()));
        timeTV.setText(timeFormatter.format(food.getDate().getTime()));
    }


    private void showCameraDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.take_photo)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        openCamera();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        builder.create().show();
    }

    private void openCamera() {
        // open camera
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        // tell camera where to store the resulting picture
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        // start camera, and wait for it to finish
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    // dictates what to do after the user takes an image, selects and image, or crops an image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        // if the camera activity is finished, obtained the uri, crop it to make it square, and send it to 'Classify' activity
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            try {
                Uri source_uri = imageUri;
                Uri dest_uri = Uri.fromFile(new File(getCacheDir(), "cropped"));
                // need to crop it to square image as CNN's always required square input
                Crop.of(source_uri, dest_uri).asSquare().start(AddFoodActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // if cropping acitivty is finished, get the resulting cropped image uri and send it to 'Classify' activity
        else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            foodImage.setImageURI(imageUri);

            imageUri = Crop.getOutput(data);
            Intent intent = new Intent(AddFoodActivity.this, ClassifyActivity.class);
            // put image data in extras to send
            intent.putExtra("resID_uri", imageUri);
            // put filename in extras
            intent.putExtra("chosen", "inception_float.tflite");
            // put model type in extras
            intent.putExtra("quant", false);
            // send other required data
            startActivityForResult(intent, REQUEST_NAME);

        } else if (requestCode == REQUEST_NAME && resultCode == RESULT_OK && data!=null) {
            food.setName(data.getStringExtra("name"));
            updateUI();
        }
    }


    private void requestPermissions() {
        // request permission to use the camera on the user's phone
        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, REQUEST_PERMISSION);
        }

        // request permission to write data (aka images) to the user's external storage of their phone
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        }

        // request permission to read data (aka images) from the user's external storage of their phone
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        }
    }

    // checks that the user has allowed all the required permission of read and write and camera.
    // If not, notify the user and close the application
    @Override
    public void onRequestPermissionsResult(
            final int requestCode,
            @NonNull final String[] permissions,
            @NonNull final int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(getApplicationContext(),
                        "This application needs read, write, and camera permissions to run. Application now closing.",
                        Toast.LENGTH_LONG);
                System.exit(0);
            }
        }
    }


    public void pickDataTime() {
        DatePickerDialog datePicker = new DatePickerDialog(
                this, this,
                food.getDate().get(Calendar.YEAR),
                food.getDate().get(Calendar.MONTH),
                food.getDate().get(Calendar.DAY_OF_MONTH)
        );
        datePicker.show();
        TimePickerDialog timePicker = new TimePickerDialog(
                this, this,
                food.getDate().get(Calendar.HOUR),
                food.getDate().get(Calendar.MINUTE),
                true
        );
        timePicker.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        food.getDate().set(year, month, dayOfMonth);
        updateUI();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        food.getDate().set(
                food.getDate().get(Calendar.YEAR),
                food.getDate().get(Calendar.MONTH),
                food.getDate().get(Calendar.DAY_OF_MONTH),
                hourOfDay,
                minute
        );
        updateUI();
    }
}
