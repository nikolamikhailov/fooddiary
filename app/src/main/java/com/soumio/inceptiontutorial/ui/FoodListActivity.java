package com.soumio.inceptiontutorial.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.soumio.inceptiontutorial.R;
import com.soumio.inceptiontutorial.data.DBHelper;
import com.soumio.inceptiontutorial.model.Food;
import com.soumio.inceptiontutorial.ui.adapter.FoodListAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class FoodListActivity extends AppCompatActivity {

    private static final int REQUEST_ADD_FOOD = 423;

    private TextView listEmptyTV;
    private RecyclerView rv;
    private FoodListAdapter adapter;
    private ArrayList<Food> foodList;

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        Log.i("M_MAIN", String.valueOf(Calendar.getInstance().getTimeInMillis()));
        dbHelper = new DBHelper(this);
        foodList = new ArrayList<>();

        initUI();

        /*SharedPreferences shPref = getPreferences(Context.MODE_PRIVATE);
        boolean isDBCreated = shPref.getBoolean("IS_DB_CREATED", false);
        if (!isDBCreated){
            dbHelper.createDB();
            SharedPreferences.Editor ed = shPref.edit();
            ed.putBoolean("IS_DB_CREATED", true);
            ed.apply();
        }*/

        (new LoadFoodAsyncTask()).execute();
    }


    private void initUI(){
        FloatingActionButton addFoodBtn = findViewById(R.id.btn_add_food);
        addFoodBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity();
            }
        });
        rv = findViewById(R.id.rv_food_list);
        listEmptyTV = findViewById(R.id.tv_list_empty);

        // создаем адаптер, передаем в него список продуктов
        adapter = new FoodListAdapter(this, foodList);
        adapter.setOnItemClickListener(new FoodListAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Toast.makeText(getApplicationContext(), getString(R.string.it_is_item)+foodList.get(position).getName(), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onItemLongClick(int position, View v) {
                showFoodDeleteDialog(position);
            }
        });
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

    }

    private void showFoodDeleteDialog(final int foodPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.sure_delete_food)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getApplicationContext(), foodList.get(foodPosition).getName()+ getString(R.string.deleted), Toast.LENGTH_LONG).show();
                        dbHelper.deleteFood(foodList.get(foodPosition));
                        foodList.remove(foodPosition);
                        updateUI();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { }
                });
        // Create the AlertDialog object and return it
        builder.create().show();

    }

    private void updateUI() {
        adapter.setFoodList(foodList);
        adapter.notifyDataSetChanged();

        if(foodList.isEmpty()){
            rv.setVisibility(View.GONE);
            listEmptyTV.setVisibility(View.VISIBLE);
        } else {
            rv.setVisibility(View.VISIBLE);
            listEmptyTV.setVisibility(View.GONE);
        }
    }

    private void startAddFoodActivity() {
        startActivityForResult(new Intent(this, AddFoodActivity.class), REQUEST_ADD_FOOD);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_FOOD && resultCode == RESULT_OK && data != null) {
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(data.getLongExtra("date", 1));
            foodList.add(new Food(
                    data.getLongExtra("id", 1),
                    data.getStringExtra("name"),
                    date
            ));
            updateUI();
        }
    }

    private class LoadFoodAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            foodList = dbHelper.loadFood();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateUI();
            findViewById(R.id.pb_main).setVisibility(View.GONE);
            findViewById(R.id.splash_main).setVisibility(View.GONE);
        }
    }

}
