package com.soumio.inceptiontutorial.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.soumio.inceptiontutorial.model.Food;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

public class DBHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "food_diary.db";
    private static final int SCHEMA = 1; // версия базы данных

    public static final String FOOD_TABLE_NAME = "food"; // название таблицы в бд
    // названия столбцов
    public static final String KEY_COLUMN_ID = "_id";
    public static final String KEY_COLUMN_NAME = "name";
    public static final String KEY_COLUMN_DATE = "date";
    private Context myContext;


    public static final String CREATE_TABLE =
            "CREATE TABLE " + FOOD_TABLE_NAME + "("
                    + KEY_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + KEY_COLUMN_NAME + " TEXT,"
                    + KEY_COLUMN_DATE + " INTEGER"
                    + ")";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, SCHEMA);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + FOOD_TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

    public Food loadFoodById(long id){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FOOD_TABLE_NAME,
                new String[]{KEY_COLUMN_ID, KEY_COLUMN_NAME, KEY_COLUMN_DATE},
                KEY_COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        Food food = null;
        // prepare note object
        while (cursor.moveToFirst()){
            String name = cursor.getString(cursor.getColumnIndex(DBHelper.KEY_COLUMN_NAME));
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(DBHelper.KEY_COLUMN_DATE)));

            food = new Food(id, name, date);
        }
        // close the db connection
        cursor.close();

        return food;
    }

    public long insertFood(Food food) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_COLUMN_NAME, food.getName());
        cv.put(KEY_COLUMN_DATE, food.getDate().getTimeInMillis());
        long id = db.insert(FOOD_TABLE_NAME, null, cv);
        db.close();
        return id;
    }

    public int updateFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_COLUMN_NAME, food.getName());
        values.put(KEY_COLUMN_DATE, food.getDate().getTimeInMillis());

        // updating row
        int id = db.update(
                FOOD_TABLE_NAME,
                values,
                KEY_COLUMN_ID + " = ?",
                new String[]{String.valueOf(food.getId())}
        );
        db.close();
        return id;
    }

    public void deleteFood(Food food) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(
                FOOD_TABLE_NAME,
                KEY_COLUMN_ID + " = ?",
                new String[]{ String.valueOf(food.getId()) }
        );
        db.close();
    }


    public ArrayList<Food> loadFood(){
        SQLiteDatabase db = getWritableDatabase();//open();
        ArrayList<Food> foodList = new ArrayList<>();

        //получаем данные из бд в виде курсора
        Cursor foodCursor = db.query(DBHelper.FOOD_TABLE_NAME,
                null, null, null, null, null, null);
        while (foodCursor.moveToNext()){
            Long id = foodCursor.getLong(foodCursor.getColumnIndex(DBHelper.KEY_COLUMN_ID));
            String name = foodCursor.getString(foodCursor.getColumnIndex(DBHelper.KEY_COLUMN_NAME));
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(foodCursor.getLong(foodCursor.getColumnIndex(DBHelper.KEY_COLUMN_DATE)));

            foodList.add(new Food(id, name, date));
        }
        foodCursor.close();

        return foodList;
    }
}
